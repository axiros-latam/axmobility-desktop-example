# AxMobility desktop example

This repository has an AxMobility example aimed for desktops.

## Building

This project uses [gradle](https://gradle.org) to be built. One can build it by
following these steps:

> **NOTE:** Remember to update the gitlab project URL in the **build.gradle** before
compiling.

* Inside the repository root directory, execute the following command: **./gradlew build**

## Running

This project can be executed by running the command: **./gradlew run**

If built successfully, it will open the following GUI:

<img src="images/main_window.png" width="400">

This main window provides two buttons: one for starting diagnostics manually and
another for canceling while running.

> **NOTE:** This project was tested under Windows and macOS.

