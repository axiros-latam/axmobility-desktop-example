package com.axiros.axmobility.desktop;

import eu.hansolo.steelseries.gauges.Radial;
import java.awt.BorderLayout;
import java.awt.Dimension;
import javax.swing.*;
import javax.swing.text.DefaultEditorKit;
import javax.swing.text.TextAction;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import com.axiros.axmobility.AxMobility;
import com.axiros.axmobility.Datamodel;

public class MainFrame extends JFrame {
    private String[] args;
    private JLabel diagnosticText;
    private JButton brun, bcancel;
    private Radial gauge;
    private JMenu jMenu;
    private final JPopupMenu popupMenu = new JPopupMenu();

    public MainFrame(String title) {
        super();
        setTitle(title);
        setSize(640,480);
        setResizable(false);
    }

    public void setArgs(String[] args) {
        this.args = args;
    }

    public void init(AxMobility axd) {
        // Menu
        jMenu = new JMenu("Edit");
        addAction(new DefaultEditorKit.CopyAction(), KeyEvent.VK_C, "Copy" );

        // Gauge
        JPanel panel = new JPanel() {
            @Override
            public Dimension getPreferredSize() {
                return new Dimension(300, 300);
            }
        };

        gauge = new Radial();
        gauge.setTitle("");
        gauge.setUnitString("");

        panel.setLayout(new BorderLayout());
        panel.add(gauge, BorderLayout.CENTER);
        add(panel);

        // Info + Buttons
        JPanel box = new JPanel();
        box.setLayout(new BoxLayout(box, BoxLayout.Y_AXIS));

        // << CPE ID
        JPanel cpeidLabel = new JPanel();
        JLabel id = new JLabel("CPE ID:");
        JTextField idText = new JTextField();
        idText.setEditable(false);
        idText.setComponentPopupMenu(popupMenu);

        try {
            String cpeid = (String)Datamodel.get("Device.DeviceInfo.X_AXIROS-COM_DeviceInfo.CPEID");
            idText.setText(cpeid);
        } catch (Exception e) {
            e.printStackTrace();
        }

        cpeidLabel.add(id);
        cpeidLabel.add(idText);
        box.add(cpeidLabel);

        // << Diagnostics information
        JPanel diagnosticsLabel = new JPanel();
        JLabel diagnostic = new JLabel("Diagnostic:");
        diagnosticText = new JLabel();
        diagnosticsLabel.add(diagnostic);
        diagnosticsLabel.add(diagnosticText);
        box.add(diagnosticsLabel);

        // << Buttons
        JPanel buttonsPanel = new JPanel();
        brun = new JButton("Run");
        brun.addActionListener(e -> {
            try {
                axd.run();
                disableRun();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });

        bcancel = new JButton("Cancel");
        bcancel.setEnabled(false);
        bcancel.addActionListener(e -> {
            try {
                axd.cancel(false);
                enableRun();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });

        buttonsPanel.add(brun);
        buttonsPanel.add(bcancel);
        box.add(buttonsPanel);
        add(box, BorderLayout.NORTH);

        pack();

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(null);
        setLocationRelativeTo(null);
        setVisible(true);
    }

    private void addAction(TextAction action, int key, String text) {
        action.putValue(AbstractAction.ACCELERATOR_KEY,
                        KeyStroke.getKeyStroke(key, InputEvent.CTRL_DOWN_MASK));

        action.putValue(AbstractAction.NAME, text);
        jMenu.add(new JMenuItem(action));
        popupMenu.add(new JMenuItem(action));
    }

    public void enableRun() {
        if (brun == null) {
            /* We are not initialized yet */
            return;
        }

        brun.setEnabled(true);
        bcancel.setEnabled(false);
    }

    public void disableRun() {
        if (brun == null) {
            /* We are not initialized yet */
            return;
        }

        brun.setEnabled(false);
        bcancel.setEnabled(true);
    }

    public void setDiagnostic(String name) {
        if (diagnosticText != null) {
            diagnosticText.setText(name);
        }
    }

    public void gaugeUpdate(double value) {
        if (gauge != null) {
            gauge.setValue(value);
        }
    }

    /*
     * Sets the gauge widget unit text.
     */
    public void gaugeSetUnit(String unit) {
        if (gauge != null) {
            gauge.setUnitString(unit);
        }
    }

    public void gaugeSetLimit(double max) {
        if (gauge != null) {
            gauge.setMaxValue(max);
        }
    }
}

